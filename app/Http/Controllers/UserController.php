<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    public function userOnlineStatus()
    {
        $data = [];
        $users = User::where('id','!=',Auth::user()->id)->get();
        foreach ($users as $user) {
            if ($user->active_status == '1') {
                array_push($data,[
                    'id'=>$user->id,
                    'nama'=>$user->name,
                    'status'=>'online',
                ]);
             
            }else{
                array_push($data,[
                    'id'=>$user->id,
                    'nama'=>$user->name,
                    'status'=>'offline',
                ]);
            }
        }
        return response()->json([
            'message' => 'Data kontak user',
            'data' =>$data
        ]);
    }

    public function chat(Request $request){
        $data = DB::table('ch_messages')->join('users','users.id','=','ch_messages.from_id')
            ->where('from_id',$request->id)
            ->orWhere('from_id',Auth::user()->id)->get();
        return response()->json([
                'message' => 'Data message user',
                'data' =>$data
            ]);
    }

    public function sendMessage(Request $request,User $user){
        try {
            //code...
            $from_id = auth()->user();
            if ($request->to_id == $from_id->id) {
                return back()->withError("You can't follow yourself");
            }else{
                $dataMesage = [
                    'id'=>substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, 36),
                    'from_id' =>$from_id->id,
                    'to_id'=>$request->to_id,
                    'body'=>$request->message,
                    'seen'=>0,
                    'created_at'=>Date('Y-m-d h:i:s'),
                    'updated_at'=>Date('Y-m-d h:i:s'),
                ];
                DB::table('ch_messages')->insert($dataMesage);
    
    
                // sending a notification
                event(new \App\Events\TestNotification('This is testing data'));
    
                return response()->json([
                    'message' => 'Sukses mengirim pesan',
                    'status' => true
                ]);
            }
            
        } catch (\Throwable $th) {
            //throw $th;
            dd($th->getMessage());
        }
      
    }
}
