@extends('layouts.index')
@section('page-content-header')
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h3 class="text-themecolor">Dashboard</h3>
    </div>
    <div class="col-md-7 align-self-center">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active"><a href="#">Dashboard</a></li>
        </ol>
    </div>
    <div>
        <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
    </div>
  
  </div>
@endsection
@section('content')
<div class="container-fluid">
    <div class="row">
                    <div class="col-lg-4">
                        <!-- Column -->
                        <div class="card"> <img class="card-img-top" src="/adminpress/assets/images/background/profile-bg.jpg" alt="Card image cap" style="max-height: 165px;">
                            <div class="card-body little-profile text-center">
                                <div class="pro-img"><img src="/adminpress/assets/images/users/4.jpg" alt="user" /></div>
                                <h3 class="m-b-0">Markarn Doe</h3>
                                <p>Web Designer &amp; Developer</p>
                                <a href="javascript:void(0)" class="m-t-10 waves-effect waves-dark btn btn-info btn-md btn-rounded">Follow</a>
                                <div class="row text-center m-t-20">
                                    <div class="col-lg-4 col-md-4 m-t-20">
                                        <h3 class="m-b-0 font-light">1099</h3><small>Articles</small></div>
                                    <div class="col-lg-4 col-md-4 m-t-20">
                                        <h3 class="m-b-0 font-light">23,469</h3><small>Followers</small></div>
                                    <div class="col-lg-4 col-md-4 m-t-20">
                                        <h3 class="m-b-0 font-light">6035</h3><small>Following</small></div>
                                    <div class="col-md-12 m-b-10"></div>
                                </div>
                            </div>
                        </div>
                        <!-- Column -->
                    </div>
                    <div class="col-md-12">
                        <div class="card m-b-0">
                            <!-- .chat-row -->
                            <div class="chat-main-box">
                                <!-- .chat-left-panel -->
                                <div class="chat-left-aside">
                                    <div class="open-panel"><i class="ti-angle-right"></i></div>
                                    <div class="chat-left-inner">
                                        <div class="form-material">
                                            <input class="form-control p-20" type="text" placeholder="Search Contact">
                                        </div>
                                        <ul class="chatonline style-none " id="chat-kontak">
                                        
                                        </ul>
                                    </div>
                                </div>
                                <!-- .chat-left-panel -->
                                <!-- .chat-right-panel -->
                                <div class="chat-right-aside">
                                    <div class="chat-main-header">
                                        <div class="p-20 b-b">
                                            <h3 class="box-title">Chat Message</h3>
                                        </div>
                                    </div>
                                    <div class="chat-rbox">
                                        <ul class="chat-list p-20" id="chat">
                                      
                                        </ul>
                                    </div>
                                    <div class="card-body b-t">
                                        <div class="row">
                                            <div class="col-8">
                                                <textarea placeholder="Type your message here" class="form-control b-0" id="message" name="message"></textarea>
                                            </div>
                                            <div class="col-4 text-right">
                                                <button type="button" class="btn btn-info btn-circle btn-lg" id="sendMessage"><i class="fa fa-paper-plane-o"></i> </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- .chat-right-panel -->
                            </div>
                            <!-- /.chat-row -->
                        </div>
                    </div>
    </div>
</div>
  
@endsection
@section('scripts')
<script src="https://js.pusher.com/8.2.0/pusher.min.js"></script>

<script>
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

var pusher = new Pusher('{{env('PUSHER_APP_KEY')}}', 
{cluster: '{{env('PUSHER_APP_CLUSTER')}}'},
{ encrypted: true }
);

Pusher.log = function(msg) {
  console.log(msg);
};




    var channel = pusher.subscribe('message-notification');
    channel.bind('push-message', function(data) {
        fetchMessageWithThisUser(to_id);
    });

$(document).ready(function() {                
    fetchKontakUser();

  

});


var to_id = '';


function fetchKontakUser(){
    html = ``;
    $.ajax({
        url : '/userOnlineStatus',
        method: 'GET',
        success: function(data) {
                            //menampilkan hasil GET
            $.each(data.data, function(item,value){
                console.log(item,value)
                html += `
                    <li>
                        <a href="#" onclick="fetchMessageWithThisUser(`+value.id+`)"><img src="/adminpress/assets/images/users/1.jpg" alt="user-img" class="img-circle"> 
                            <span>`+value.nama+` 
                                <small class="text-secondary">`+value.status+`</small>
                            </span>
                        </a>
                    </li>
                `;
            })
            $("#chat-kontak").append(html);

        }
    })
                
}

function fetchMessageWithThisUser(id){
    to_id = id;
    html = ``;
    $("#chat").html("");
    $.ajax({
        url : '/chat',
        method: 'GET',
        data:{
            'id' : id,
        },
        success: function(data) {
            html = ``;
            if(data.data.length > 0) {
            $.each(data.data, function(item,value){
                if(id == value.from_id){
                    html += `
                    <li>
                                                    <div class="chat-img"><img src="/adminpress/assets/images/users/2.jpg" alt="user" /></div>
                                                    <div class="chat-content">
                                                        <h5 style="color:white;">`+value.name+`</h5>
                                                        <div class="box bg-light-info">`+value.body+`</div>
                                                    </div>
                                                    <div class="chat-time">10:57 am</div>
                                                </li>
                    `;
                }else{
                    html += `
                    <li class="reverse">
                                                
                                                <div class="chat-content">
                                                    <h5 style="color:white;">`+value.name+`</h5>
                                                    <div class="box bg-light-inverse">`+value.body+`</div>
                                                </div>
                                                <div class="chat-img"><img src="/adminpress/assets/images/users/5.jpg" alt="user" /></div>
                                                <div class="chat-time">10:57 am</div>
                                            </li>
                    `;
                }
                  
                });

                $("#chat").append(html);
            }else{
                $("#chat").html("");
            }

        }
    })

}

$('#sendMessage').click(function(e) {
        e.preventDefault();

        //define variable
        let message   = $('#message').val();
        let token   = $("meta[name='csrf-token']").attr("content");
        
        //ajax
        $.ajax({

            url: `/sendMessage`,
            type: "POST",
            cache: false,
            data: {
                "message": message,
                "to_id": to_id,
                "_token": token
            },
            success:function(response){
                console.log(response);
                //show success message
                // fetchMessageWithThisUser(to_id)

            },
            error:function(error){
             

            }

        });

    });
</script>

@endsection